use std::io;

fn main() {
    // Read input from the user
    println!("Enter the duration as hours:minutes:seconds");
    let mut input = String::new();
    io::stdin().read_line(&mut input).unwrap();

    // Parse the input into hours, minutes, and seconds
    let parts: Vec<&str> = input.trim().split(':').collect();
    let hours: u64 = parts[0].parse().unwrap();
    let minutes: u64 = parts[1].parse().unwrap();
    let seconds: u64 = parts[2].parse().unwrap();

    // Calculate the duration in ticks
    let duration_ticks = (((hours * 3600) + (minutes * 60) + seconds) * 20);
    let items = ((duration_ticks - 2) / 8) + 1;

    // Print the result
    println!("Put {} items into your hopper clock for {} hours {} minutes {} seconds", items, hours, minutes, seconds);
}
